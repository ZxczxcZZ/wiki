package com.jiawa.wiki;

import com.jiawa.wiki.domain.Doc;
import com.jiawa.wiki.domain.DocExample;
import com.jiawa.wiki.mapper.DocMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

@SpringBootTest
class WikiApplicationTests {

    @Test
    void contextLoads() {
    }


    @Autowired
    DocMapper docMapper;

    @Test
    void docDelete(){

        this.delete((long)7);

    }


    //根据id删除doc中的数据，并递归删除子节点
    public void delete(Long id){
        //首先删除这个id的doc
        docMapper.deleteByPrimaryKey(id);

        //判断这个id是不是其他的父id
        DocExample docExample = new DocExample();
        docExample.createCriteria().andParentEqualTo(id);

        List<Doc> docList = docMapper.selectByExample(docExample);

        //没有子类的话则直接返回结束
        if (docList==null || docList.size()<=0){
            return ;
        }

        //有子分类直接删除子分类，此处直接递归
        for (Doc doc : docList) {
            this.delete(doc.getId());
        }

    }

}
