package com.jiawa.wiki.obj.dto;

import lombok.Data;

@Data
public class UserQueryDTO {
    private Long id;

    private String loginName;

    private String name;

    private String password;
}