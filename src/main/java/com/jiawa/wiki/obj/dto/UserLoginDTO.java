package com.jiawa.wiki.obj.dto;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserLoginDTO {
    private Long id;

    @NotNull(message = "loginName不能为空")
    private String loginName;

    @NotNull(message = "name不能为空")
    private String name;

    private String token;

}