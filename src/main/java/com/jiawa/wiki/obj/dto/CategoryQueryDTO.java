package com.jiawa.wiki.obj.dto;

import lombok.Data;

@Data
public class CategoryQueryDTO {
    private Long id;

    private Long parent;

    private String name;

    private Integer sort;

}