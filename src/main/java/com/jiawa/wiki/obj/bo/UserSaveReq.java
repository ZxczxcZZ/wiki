package com.jiawa.wiki.obj.bo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserSaveReq {
    private Long id;

    @NotNull(message = "loginName不能为空")
    private String loginName;

    @NotNull(message = "name不能为空")
    private String name;

    @NotNull(message = "password不能为空")
    private String password;
}