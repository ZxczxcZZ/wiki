package com.jiawa.wiki.obj.bo;

import lombok.Data;

@Data
public class EbookQueryReq extends PageReq {

    private Long id;

    private String name;

    private Long category2Id;

}
