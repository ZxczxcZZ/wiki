package com.jiawa.wiki.obj.bo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserResetPasswordReq {
    private Long id;


    @NotNull(message = "password不能为空")
    private String password;
}