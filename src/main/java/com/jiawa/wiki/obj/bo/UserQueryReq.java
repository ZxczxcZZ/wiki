package com.jiawa.wiki.obj.bo;

import lombok.Data;

@Data
public class UserQueryReq extends PageReq{
    private String loginName;

}