package com.jiawa.wiki.obj.bo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class DocSaveReq {
    private Long id;

    @NotNull(message = "ebookId不能为空")
    private Long ebookId;

    @NotNull(message = "parent不能为空")
    private Long parent;

    @NotNull(message = "name不能为空")
    private String name;

    @NotNull(message = "sort不能为空")
    private Integer sort;

    @NotNull(message = "content不能为空")
    private String content;

    private Integer viewCount;

    private Integer voteCount;

}