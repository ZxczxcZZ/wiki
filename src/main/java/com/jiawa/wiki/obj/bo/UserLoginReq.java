package com.jiawa.wiki.obj.bo;

import lombok.Data;

import javax.validation.constraints.NotNull;

@Data
public class UserLoginReq {

    @NotNull(message = "loginName不能为空")
    private String loginName;


    @NotNull(message = "password不能为空")
    private String password;
}