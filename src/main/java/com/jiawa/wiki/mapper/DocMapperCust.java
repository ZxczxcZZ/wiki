package com.jiawa.wiki.mapper;

import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface DocMapperCust {
     void increaseViewCount(Long id);

     void increaseVoteCount(Long id);

     void updateEbookInfo();
}
