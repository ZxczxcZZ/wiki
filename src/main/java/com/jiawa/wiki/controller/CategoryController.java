package com.jiawa.wiki.controller;

import com.jiawa.wiki.obj.bo.CategoryQueryReq;
import com.jiawa.wiki.obj.bo.CategorySaveReq;
import com.jiawa.wiki.obj.dto.CategoryQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.service.CategoryService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.List;


@RestController
@RequestMapping("/category")
public class CategoryController {
    @Autowired
    private CategoryService categoryService;

    @GetMapping("/list")
    public CommonResp list(@Valid CategoryQueryReq req){
        CommonResp<PageResp<CategoryQueryDTO>> resp = new CommonResp<>();
        PageResp<CategoryQueryDTO> result = categoryService.list(req);
        resp.setContent(result);
        return resp;
    }

    @GetMapping("/all")
    public CommonResp all(){
        CommonResp<List<CategoryQueryDTO>> resp = new CommonResp<>();
        List<CategoryQueryDTO> all = categoryService.all();
        resp.setContent(all);
        return resp;
    }

    @PostMapping("/save")
    public CommonResp list(CategorySaveReq req){
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = categoryService.save(req);
        resp.setContent(result);
        return resp;
    }

    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id){
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = categoryService.delete(id);
        resp.setContent(result);
        return resp;
    }
}
