package com.jiawa.wiki.controller;

import com.jiawa.wiki.obj.bo.EbookQueryReq;
import com.jiawa.wiki.obj.bo.EbookSaveReq;
import com.jiawa.wiki.obj.dto.EbookQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.service.EbookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;


@RestController
@RequestMapping("/ebook")
public class EbookController {
    @Autowired
    private EbookService ebookService;

    @GetMapping("/list")
    public CommonResp list(@Valid EbookQueryReq req){
        CommonResp<PageResp<EbookQueryDTO>> resp = new CommonResp<>();
        PageResp<EbookQueryDTO> result = ebookService.list(req);
        resp.setContent(result);
        return resp;
    }

    @PostMapping("/save")
    public CommonResp list(EbookSaveReq req){
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = ebookService.save(req);
        resp.setContent(result);
        return resp;
    }

    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id){
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = ebookService.delete(id);
        resp.setContent(result);
        return resp;
    }
}
