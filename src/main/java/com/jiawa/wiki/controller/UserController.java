package com.jiawa.wiki.controller;

import com.alibaba.fastjson.JSON;
import com.jiawa.wiki.obj.bo.UserLoginReq;
import com.jiawa.wiki.obj.bo.UserQueryReq;
import com.jiawa.wiki.obj.bo.UserResetPasswordReq;
import com.jiawa.wiki.obj.bo.UserSaveReq;
import com.jiawa.wiki.obj.dto.UserLoginDTO;
import com.jiawa.wiki.obj.dto.UserQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.service.UserService;
import com.jiawa.wiki.util.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.util.DigestUtils;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.concurrent.TimeUnit;


@RestController
@RequestMapping("/user")
@Slf4j
public class UserController {
    @Autowired
    private UserService userService;

    @Autowired
    private RedisTemplate redisTemplate;

    @Autowired
    private SnowFlake snowFlake;

    @GetMapping("/list")
    public CommonResp list(@Valid UserQueryReq req){
        System.out.println(req);
        CommonResp<PageResp<UserQueryDTO>> resp = new CommonResp<>();
        PageResp<UserQueryDTO> result = userService.list(req);
        resp.setContent(result);
        return resp;
    }

    @PostMapping("/save")
    public CommonResp list(UserSaveReq req){
        //md5加密
        req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = userService.save(req);
        resp.setContent(result);
        return resp;
    }

    @PostMapping("/reset-password")
    public CommonResp resetPassword(UserResetPasswordReq req){
        //md5加密
        req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = userService.resetPassword(req);
        resp.setContent(result);
        return resp;
    }

    @PostMapping("/login")
    public CommonResp login(UserLoginReq req){
        //md5加密
        req.setPassword(DigestUtils.md5DigestAsHex(req.getPassword().getBytes()));
        CommonResp<UserLoginDTO> resp = new CommonResp<>();
        UserLoginDTO userLoginDTO = userService.login(req);
        //生成单点登录token，并放入redis中
        Long token = snowFlake.nextId();
        userLoginDTO.setToken(token.toString());
        log.info("生成token:"+token+"并存入redis中");
        redisTemplate.opsForValue().set(token.toString(), JSON.toJSONString(userLoginDTO),3600*24, TimeUnit.SECONDS);
        resp.setContent(userLoginDTO);
        return resp;
    }

    @DeleteMapping("/delete/{id}")
    public CommonResp delete(@PathVariable Long id){
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = userService.delete(id);
        resp.setContent(result);
        return resp;
    }


    @GetMapping("/logout/{token}")
    public CommonResp delete(@PathVariable String token){
        CommonResp<Boolean> resp = new CommonResp<>();
        log.info("从redis中删除token");
        Boolean result = redisTemplate.delete(token);
        resp.setContent(result);
        return resp;
    }
}
