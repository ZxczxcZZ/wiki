package com.jiawa.wiki.controller;

import com.jiawa.wiki.obj.bo.DocQueryReq;
import com.jiawa.wiki.obj.bo.DocSaveReq;
import com.jiawa.wiki.obj.dto.DocQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.resp.CommonResp;
import com.jiawa.wiki.service.DocService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;
import java.util.List;


@RestController
@RequestMapping("/doc")
public class DocController {
    @Autowired
    private DocService docService;

    @GetMapping("/list")
    public CommonResp list(@Valid DocQueryReq req){
        CommonResp<PageResp<DocQueryDTO>> resp = new CommonResp<>();
        PageResp<DocQueryDTO> result = docService.list(req);
        resp.setContent(result);
        return resp;
    }

    @GetMapping("/all/{ebookId}")
    public CommonResp all(@PathVariable Long ebookId){
        CommonResp<List<DocQueryDTO>> resp = new CommonResp<>();
        List<DocQueryDTO> all = docService.all(ebookId);
        resp.setContent(all);
        return resp;
    }

    @PostMapping("/save")
    public CommonResp list(DocSaveReq req){
        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = docService.save(req);
        resp.setContent(result);
        return resp;
    }

    @DeleteMapping("/delete/{idsStr}")
    public CommonResp delete(@PathVariable String idsStr){

        String[] arr = idsStr.split(",");

        List<Long> ids=new ArrayList<>();

        for (String s : arr) {
            ids.add(Long.parseLong(s));
        }

        CommonResp<Integer> resp = new CommonResp<>();
        Integer result = docService.delete(ids);
        resp.setContent(result);
        return resp;
    }


    @GetMapping("/find-content/{id}")
    public CommonResp findContent(@PathVariable Long id){
        CommonResp<String> resp = new CommonResp<>();
       String content = docService.findContent(id);
        resp.setContent(content);
        return resp;
    }

    @GetMapping("/vote/{id}")
    public CommonResp vote(@PathVariable Long id){
        CommonResp<Object> resp = new CommonResp<>();
        docService.vote(id);
        return resp;
    }

}
