package com.jiawa.wiki.controller;

import com.jiawa.wiki.execption.BusinessException;
import com.jiawa.wiki.resp.CommonResp;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.validation.BindException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

/**
 * 统一异常处理、数据预处理等
 */
@ControllerAdvice
public class ControllerExceptionHandler {

    private static final Logger LOG = LoggerFactory.getLogger(ControllerExceptionHandler.class);

    /**
     * 校验异常统一处理
     *
     * @param
     * @return
     */
    @ExceptionHandler(value = BindException.class) //捕获数据校验绑定异常 BindException
    @ResponseBody
    public CommonResp validExceptionHandler(BindException e) {
        //因为我们的程序还没走到Controller就异常结束了，所以我们新建了一个统一异常类来专门处理，然后模拟一个controller 返回CommonResp
        CommonResp commonResp = new CommonResp();
        LOG.warn("参数校验失败：{}", e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        commonResp.setSuccess(false);
        commonResp.setMessage(e.getBindingResult().getAllErrors().get(0).getDefaultMessage());
        return commonResp;
    }

    /**
     * 校验异常统一处理
     *
     * @param
     * @return
     */
    @ExceptionHandler(value = BusinessException.class) //捕获数据校验绑定异常 BindException
    @ResponseBody
    public CommonResp validExceptionHandler(BusinessException e) {
        //因为我们的程序还没走到Controller就异常结束了，所以我们新建了一个统一异常类来专门处理，然后模拟一个controller 返回CommonResp
        CommonResp commonResp = new CommonResp();
        LOG.warn("业务异常：{}", e.getCode().getDesc());
        commonResp.setSuccess(false);
        commonResp.setMessage(e.getCode().getDesc());
        return commonResp;
    }


    /**
     * 校验异常统一处理
     *
     * @param e
     * @return
     */
    @ExceptionHandler(value = Exception.class)
    @ResponseBody
    public CommonResp validExceptionHandler(Exception e) {
        CommonResp commonResp = new CommonResp();
        LOG.error("系统异常：", e);
        commonResp.setSuccess(false);
        commonResp.setMessage("系统出现异常，请联系管理员");
        return commonResp;
    }

}