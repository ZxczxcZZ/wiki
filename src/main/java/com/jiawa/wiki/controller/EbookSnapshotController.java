//package com.jiawa.wiki.controller;
//
//import com.jiawa.wiki.resp.CommonResp;
//import com.jiawa.wiki.service.EbookSnapshotService;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.GetMapping;
//import org.springframework.web.bind.annotation.PathVariable;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.util.List;
//
//@RestController
//@RequestMapping("/ebook-snapshot")
//public class EbookSnapshotController {
//    @Autowired
//    private EbookSnapshotService ebookSnapshotService;
//
//    @GetMapping("/get-statistic")
//    public CommonResp getStatistic(){
//        List<StatisticResp> statisticResp =ebookSnapshotService.getStatistic();
//        CommonResp<List<StatisticResp>> resp = new CommonResp<>();
//        resp.setContent(statisticResp);
//        return resp;
//    }
//}
