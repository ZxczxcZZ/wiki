package com.jiawa.wiki.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jiawa.wiki.domain.Content;
import com.jiawa.wiki.domain.Doc;
import com.jiawa.wiki.domain.DocExample;
import com.jiawa.wiki.domain.Ebook;
import com.jiawa.wiki.execption.BusinessException;
import com.jiawa.wiki.execption.BusinessExceptionCode;
import com.jiawa.wiki.mapper.ContentMapper;
import com.jiawa.wiki.mapper.DocMapper;
import com.jiawa.wiki.mapper.DocMapperCust;
import com.jiawa.wiki.obj.bo.DocQueryReq;
import com.jiawa.wiki.obj.bo.DocSaveReq;
import com.jiawa.wiki.obj.dto.DocQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.RedisUtil;
import com.jiawa.wiki.util.RequestContext;
import com.jiawa.wiki.util.SnowFlake;
import com.jiawa.wiki.websocket.WebSocketServer;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@Slf4j
public class DocService {

    @Autowired
    private DocMapper docMapper;

    @Autowired
    private SnowFlake snowFlake;

    @Autowired
    private ContentMapper contentMapper;

    @Autowired
    private DocMapperCust docMapperCust;

    @Autowired
    private RedisUtil redisUtil;


    @Autowired
    private WsService wsService;

    public PageResp<DocQueryDTO> list(DocQueryReq bo){

        PageResp<DocQueryDTO> pageResp = new PageResp<>();
        DocExample docExample = new DocExample();
        docExample.setOrderByClause("sort asc");



        //会作用在第一个查询的数据上
        Page<Doc> page = PageHelper.startPage(bo.getPage(), bo.getSize());
        List<Doc> docList = docMapper.selectByExample(docExample);
        long total = page.getTotal();
        int pages = page.getPages();
        log.info("总记录数:"+total);
        log.info("总页数:"+pages);
        //将po集合转换为dto集合
        List<DocQueryDTO> docQueryDTOS = CopyUtil.copyList(docList, DocQueryDTO.class);

        pageResp.setTotal(total);
        pageResp.setList(docQueryDTOS);
        return pageResp;
    }

    public List<DocQueryDTO> all(Long ebookId){

        DocExample docExample = new DocExample();
        docExample.setOrderByClause("sort asc");

        docExample.createCriteria().andEbookIdEqualTo(ebookId);
        //会作用在第一个查询的数据上
        List<Doc> docList = docMapper.selectByExample(docExample);
        //将po集合转换为dto集合
        List<DocQueryDTO> docQueryDTOS = CopyUtil.copyList(docList, DocQueryDTO.class);

        return docQueryDTOS;
    }

    //保存，既支持新增也支撑更新
    @Transactional
    public Integer save(DocSaveReq req){
        Doc doc = CopyUtil.copy(req, Doc.class);
        Content content = CopyUtil.copy(req, Content.class);

        //判断是更新还是新增
        int result;
        if (req.getId()==null){
            //新增
            doc.setViewCount(0);
            doc.setVoteCount(0);
            System.out.println("看看我执行了吗？");
            long id = snowFlake.nextId();
            doc.setId(id);
            result = docMapper.insert(doc);
            content.setId(id);
            contentMapper.insert(content);
             if(result>0){
                 System.out.println("新增成功！");
             }
        }else {
            //更新
             result = docMapper.updateByPrimaryKey(doc);
             contentMapper.updateByPrimaryKeyWithBLOBs(content);
            if(result>0){
                System.out.println("更新成功！");
            }
        }

        return result;

    }

    public Integer delete(Long id){
        int result = docMapper.deleteByPrimaryKey(id);

        if (result>0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败");
        }

        return result;
    }

    public Integer delete(List<Long> ids){

        DocExample docExample = new DocExample();
        docExample.createCriteria().andIdIn(ids);

        int result = docMapper.deleteByExample(docExample);

        if (result>0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败");
        }

        return result;
    }

    public String findContent(Long id){
        Content content = contentMapper.selectByPrimaryKey(id);
        //文档阅读数加一
        docMapperCust.increaseViewCount(id);
        if (content==null){
            return "";
        }
        return content.getContent();
    }

    public void vote(Long id){
        //ip+doc.id作为key，24小时不能重复
        String ip= RequestContext.getRemoteAddr();
        if (redisUtil.validateRepeat("DOC_VOTE_"+id+"_"+ip,3600*24)){
            docMapperCust.increaseVoteCount(id);
        }else {
            throw new BusinessException((BusinessExceptionCode.VOTE_REPEAT));
        }
        //推送消息
        Doc doc = docMapper.selectByPrimaryKey(id);
        wsService.sendInfo("【"+doc.getName()+"】被点赞!");
    }



    public void updateEbookInfo(){
        docMapperCust.updateEbookInfo();
    }

}
