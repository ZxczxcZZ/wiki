package com.jiawa.wiki.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jiawa.wiki.domain.Category;
import com.jiawa.wiki.domain.CategoryExample;
import com.jiawa.wiki.mapper.CategoryMapper;
import com.jiawa.wiki.obj.bo.CategoryQueryReq;
import com.jiawa.wiki.obj.bo.CategorySaveReq;
import com.jiawa.wiki.obj.dto.CategoryQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class CategoryService {

    @Autowired
    private CategoryMapper categoryMapper;

    @Autowired
    SnowFlake snowFlake;

    public PageResp<CategoryQueryDTO> list(CategoryQueryReq bo){

        PageResp<CategoryQueryDTO> pageResp = new PageResp<>();
        CategoryExample categoryExample = new CategoryExample();
        categoryExample.setOrderByClause("sort asc");



        //会作用在第一个查询的数据上
        Page<Category> page = PageHelper.startPage(bo.getPage(), bo.getSize());
        List<Category> categoryList = categoryMapper.selectByExample(categoryExample);
        long total = page.getTotal();
        int pages = page.getPages();
        log.info("总记录数:"+total);
        log.info("总页数:"+pages);
        //将po集合转换为dto集合
        List<CategoryQueryDTO> categoryQueryDTOS = CopyUtil.copyList(categoryList, CategoryQueryDTO.class);

        pageResp.setTotal(total);
        pageResp.setList(categoryQueryDTOS);
        return pageResp;
    }

    public List<CategoryQueryDTO> all(){

        CategoryExample categoryExample = new CategoryExample();
        categoryExample.setOrderByClause("sort asc");

        //会作用在第一个查询的数据上
        List<Category> categoryList = categoryMapper.selectByExample(categoryExample);
        //将po集合转换为dto集合
        List<CategoryQueryDTO> categoryQueryDTOS = CopyUtil.copyList(categoryList, CategoryQueryDTO.class);

        return categoryQueryDTOS;
    }

    //保存，既支持新增也支撑更新
    public Integer save(CategorySaveReq req){
        Category category = CopyUtil.copy(req, Category.class);

        //判断是更新还是新增
        int result;
        if (req.getId()==null){
            //新增
            System.out.println("看看我执行了吗？");
            long id = snowFlake.nextId();
           category.setId(id);
            result = categoryMapper.insert(category);
             if(result>0){
                 System.out.println("新增成功！");
             }
        }else {
            //更新
             result = categoryMapper.updateByPrimaryKey(category);
            if(result>0){
                System.out.println("更新成功！");
            }
        }

        return result;

    }

    public Integer delete(Long id){
        int result = categoryMapper.deleteByPrimaryKey(id);

        if (result>0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败");
        }

        return result;
    }

}
