package com.jiawa.wiki.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jiawa.wiki.domain.User;
import com.jiawa.wiki.domain.UserExample;
import com.jiawa.wiki.execption.BusinessException;
import com.jiawa.wiki.execption.BusinessExceptionCode;
import com.jiawa.wiki.mapper.UserMapper;
import com.jiawa.wiki.obj.bo.UserLoginReq;
import com.jiawa.wiki.obj.bo.UserQueryReq;
import com.jiawa.wiki.obj.bo.UserResetPasswordReq;
import com.jiawa.wiki.obj.bo.UserSaveReq;
import com.jiawa.wiki.obj.dto.UserLoginDTO;
import com.jiawa.wiki.obj.dto.UserQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@Slf4j
public class UserService {

    @Autowired
    private UserMapper userMapper;

    @Autowired
    SnowFlake snowFlake;

    public PageResp<UserQueryDTO> list(UserQueryReq bo){

        PageResp<UserQueryDTO> pageResp = new PageResp<>();
        UserExample userExample = new UserExample();

        if(bo.getLoginName()!=null && bo.getLoginName().length()>0){
            userExample.createCriteria().andLoginNameEqualTo(bo.getLoginName());
        }


        //会作用在第一个查询的数据上
        Page<User> page = PageHelper.startPage(bo.getPage(), bo.getSize());
        List<User> userList = userMapper.selectByExample(userExample);
        long total = page.getTotal();
        int pages = page.getPages();
        log.info("总记录数:"+total);
        log.info("总页数:"+pages);
        //将po集合转换为dto集合
        List<UserQueryDTO> userQueryDTOS = CopyUtil.copyList(userList, UserQueryDTO.class);

        pageResp.setTotal(total);
        pageResp.setList(userQueryDTOS);
        return pageResp;
    }

    //保存，既支持新增也支撑更新
    public Integer save(UserSaveReq req){
        User user = CopyUtil.copy(req, User.class);

        //判断是更新还是新增
        int result;
        if (req.getId()==null){
            //新增
            User res = this.selectByLoginName(req.getLoginName());
            if (res!=null){
                //用户名已存在
                throw new BusinessException(BusinessExceptionCode.USER_LOGIN_NAME_EXIST);
            }
            long id = snowFlake.nextId();
           user.setId(id);
            result = userMapper.insert(user);
             if(result>0){
                 System.out.println("新增成功！");
             }
        }else {
            //更新
            //不允许修改loginName，手动清空一下
            user.setLoginName(null);
            //加上Selective，表示user中这个属性有值我才去更新，没有则不更新
             result = userMapper.updateByPrimaryKeySelective(user);
            if(result>0){
                System.out.println("更新成功！");
            }
        }

        return result;

    }

    public Integer delete(Long id){
        int result = userMapper.deleteByPrimaryKey(id);

        if (result>0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败");
        }

        return result;
    }

    public User selectByLoginName(String loginName){
        UserExample userExample=new UserExample();
        userExample.createCriteria().andLoginNameEqualTo(loginName);
        List<User> users = userMapper.selectByExample(userExample);
        if (users==null || users.size()<=0){
            return null;
        }
        return users.get(0);
    }

    public Integer resetPassword(UserResetPasswordReq req){
        User user = CopyUtil.copy(req, User.class);
        int res = userMapper.updateByPrimaryKeySelective(user);
        return res;
    }

    public UserLoginDTO login(UserLoginReq req){
        User user = selectByLoginName(req.getLoginName());

        //用户名不存在
        if (user==null){
            log.info("用户名不存在");
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }

        //密码错误
        if (!user.getPassword().equals(req.getPassword())) {
            log.info("密码错误");
            throw new BusinessException(BusinessExceptionCode.LOGIN_USER_ERROR);
        }

        log.info("登录成功！");
        UserLoginDTO loginDTO = CopyUtil.copy(user, UserLoginDTO.class);
        return loginDTO;
    }
}
