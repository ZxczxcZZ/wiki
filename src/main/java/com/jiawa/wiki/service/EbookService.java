package com.jiawa.wiki.service;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.jiawa.wiki.domain.Ebook;
import com.jiawa.wiki.domain.EbookExample;
import com.jiawa.wiki.mapper.EbookMapper;
import com.jiawa.wiki.obj.bo.EbookQueryReq;
import com.jiawa.wiki.obj.bo.EbookSaveReq;
import com.jiawa.wiki.obj.dto.EbookQueryDTO;
import com.jiawa.wiki.obj.dto.PageResp;
import com.jiawa.wiki.util.CopyUtil;
import com.jiawa.wiki.util.SnowFlake;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import java.util.List;

@Service
@Slf4j
public class EbookService {

    @Autowired
    private EbookMapper ebookMapper;

    @Autowired
    SnowFlake snowFlake;

    public PageResp<EbookQueryDTO> list(EbookQueryReq bo){

        PageResp<EbookQueryDTO> pageResp = new PageResp<>();
        EbookExample ebookExample = new EbookExample();

        if(bo.getName()!=null && bo.getName().length()>0){
            ebookExample.createCriteria().andNameLike("%"+bo.getName()+"%");
        }

        if (bo.getCategory2Id()!=null){
            ebookExample.createCriteria().andCategory2IdEqualTo(bo.getCategory2Id());
        }

        //会作用在第一个查询的数据上
        Page<Ebook> page = PageHelper.startPage(bo.getPage(), bo.getSize());
        List<Ebook> ebookList = ebookMapper.selectByExample(ebookExample);
        long total = page.getTotal();
        int pages = page.getPages();
        log.info("总记录数:"+total);
        log.info("总页数:"+pages);
        //将po集合转换为dto集合
        List<EbookQueryDTO> ebookQueryDTOS = CopyUtil.copyList(ebookList, EbookQueryDTO.class);

        pageResp.setTotal(total);
        pageResp.setList(ebookQueryDTOS);
        return pageResp;
    }

    //保存，既支持新增也支撑更新
    public Integer save(EbookSaveReq req){
        Ebook ebook = CopyUtil.copy(req, Ebook.class);

        //判断是更新还是新增
        int result;
        if (req.getId()==null){
            //新增
            System.out.println("看看我执行了吗？");
            long id = snowFlake.nextId();
           ebook.setId(id);
            result = ebookMapper.insert(ebook);
             if(result>0){
                 System.out.println("新增成功！");
             }
        }else {
            //更新
             result = ebookMapper.updateByPrimaryKey(ebook);
            if(result>0){
                System.out.println("更新成功！");
            }
        }

        return result;

    }

    public Integer delete(Long id){
        int result = ebookMapper.deleteByPrimaryKey(id);

        if (result>0){
            System.out.println("删除成功！");
        }else {
            System.out.println("删除失败");
        }

        return result;
    }

}
